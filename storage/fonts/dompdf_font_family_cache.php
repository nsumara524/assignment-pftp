<?php return function ($fontDir, $rootDir) {
return array (
  'sans-serif' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold' => $rootDir . '/lib/fonts/ZapfDingbats',
    'italic' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold_italic' => $rootDir . '/lib/fonts/ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $rootDir . '/lib/fonts/Symbol',
    'bold' => $rootDir . '/lib/fonts/Symbol',
    'italic' => $rootDir . '/lib/fonts/Symbol',
    'bold_italic' => $rootDir . '/lib/fonts/Symbol',
  ),
  'serif' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSans-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSans-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSans-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSansMono-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSansMono-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSansMono-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSerif-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSerif-BoldItalic',
    'italic' => $rootDir . '/lib/fonts/DejaVuSerif-Italic',
    'normal' => $rootDir . '/lib/fonts/DejaVuSerif',
  ),
  'montserrat' => array(
    '200' => $fontDir . '/montserrat_200_f91b1435ed16f0eae31cc6b9d57e9a10',
    'normal' => $fontDir . '/montserrat_normal_ae7fa7391ffa7c2e4d7b0000fc6cfe49',
    'bold' => $fontDir . '/montserrat_bold_28f0edcd095c74e3bd6080968046a002',
  ),
  'fontawesome' => array(
    'normal' => $fontDir . '/fontawesome_normal_d726fe9f0eedcde03c194e171054f6d7',
  ),
);
}; ?>