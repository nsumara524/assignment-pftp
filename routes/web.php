<?php

use App\Http\Controllers\PrintController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group( ['middleware' => 'auth'],function () {

Route::get('index',[ProfileController::class,'index'])->name('profile-index');
Route::get('create', [ProfileController::class, 'create'])->name('profile-create');
Route::post('store', [ProfileController::class, 'store'])->name('profile-store');
Route::get('edit/{id?}', [ProfileController::class,'edit'])->name('profile-edit');
Route::post('update/{id?}', [ProfileController::class, 'update'])->name('profile-update');
Route::get('delete/{id?}', [ProfileController::class, 'destroy'])->name('profile-destroy');
Route::get('logout', [ProfileController::class, 'logout'])->name('logout');
Route::get('print-invoice/{id}',[PrintController::class,'invoice'])->name('print-invoice');
Route::get('invoice/{id}', [PrintController::class, 'print_invoice'])->name('invoice');

});