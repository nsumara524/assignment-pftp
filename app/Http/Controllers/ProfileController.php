<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        if(!$user->phone && !$user->address && !$user->dob && !$user->language) {
            return redirect(url('/create'));
        }
        return view('profile.index', compact('user'));
    }
    public function create()
    {
        $user = Auth::user();
        return view('profile.create', compact('user'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'dob' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'language' => 'required',
            'file' => 'required',

        ]);
        $user = Auth::user();
        if ($request->hasFile('file')) {
            $path = $request->file('file');
            $target = '/image';
            $file = Storage::putfile($target, $path);

        }
        $user->update([
            'dob' => $request->age,
            'phone' => $request->phone,
            'address' => $request->address,
            'language' => $request->language,
            'avatar' => $file,

        ]);
        return redirect()->route('profile-index')->with('data inserted Sucessfully');
    }
    public function edit()
    {
        $user = Auth::user();
        return view('profile.edit', compact('user'));
    }
    public function update(Request $request, $id)
    {
        //    dd($request->all());
        $user = Auth::user();
        $userDate = User::where('id', $user->id)->first();
        $userDate->name = $request->name;
        $userDate->dob = @$request->dob;
        $userDate->phone = $request->phone;
        $userDate->address = $request->address;
        $userDate->language = $request->language;

        if ($request->hasFile('file')) {
            $path = $request->file('file');
            $target = '/image';
            $file = Storage::putfile($target, $path);
            // dd($file);
            $userDate->avatar = $file;
        }
        $userDate->save();
        return redirect()->route('profile-index')->with('data edit ');
    }
    public function destroy(Request $request, $id)
    {
        $data =  User::find($id);
        $data->delete();
        $request->session()->put('status', true);
        return back();
    }
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/login');
    }
}
