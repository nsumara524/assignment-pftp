<?php

namespace App\Http\Controllers;

use App\Models\User;
use PDF;
use Illuminate\Http\Request;

class PrintController extends Controller
{
    public function invoice($id)
    {
        if (User::where('id', $id)->exists()) {
            $user = User::find($id);
            $data = [
                'user' => $user,
            ];
            PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
            $pdf = PDF::loadView('profile.invoke', $data);
            return $pdf->download('fundaofwebit.pdf');
        } else {
            return redirect()->back()->with('profile not exists');
        }
    }
    public function print_invoice($id){
        if (User::where('id', $id)->exists()) {
            $user = User::find($id);
            $data = [
                'user' => $user,
            ];
        return view('profile.invoice',compact('user'));
    }
} 
}

// acha main btata hu  print ka butto
